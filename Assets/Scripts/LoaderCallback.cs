using System;
using UnityEngine;

public class LoaderCallback : MonoBehaviour
{
    private bool isFirstUpdate = true;

    private void Update()
    {
        if (isFirstUpdate)
        {
            Debug.Log("First update");
            isFirstUpdate = false;
            
            Loader.LoaderCallback();
        }
    }
}
