using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button quitButton;

    private void Awake()
    {
        playButton.onClick.AddListener(() =>
        {
            Debug.Log("Play Button Clicked");
            Loader.Load(Loader.Scene.GameScene);
        });
        
        quitButton.onClick.AddListener(Application.Quit);

        Time.timeScale = 1f;
    }
}
