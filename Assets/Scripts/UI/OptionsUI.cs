using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    public static OptionsUI Instance { get; private set; }
    
    [SerializeField] private Button soundFxButton;
    [SerializeField] private Button musicButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private TextMeshProUGUI soundFxText;
    [SerializeField] private TextMeshProUGUI musicText;

    private void Awake()
    {
        Instance = this;
        
        soundFxButton.onClick.AddListener(() =>
        {
            SoundManager.Instance.ChangeVolume();
            UpdateVisual();
        });
        
        musicButton.onClick.AddListener(() =>
        {
            MusicManager.Instance.ChangeVolume();
            UpdateVolumeVisual();
        });
        
        closeButton.onClick.AddListener(Hide);
    }

    private void UpdateVisual()
    {
        soundFxText.text = $"Sound FX: {Mathf.Round(SoundManager.Instance.ReturnVolume() * 10f)}";
    }

    private void UpdateVolumeVisual()
    {
        musicText.text = $"Volume: { Mathf.Round(MusicManager.Instance.Volume * 10f)}";
    }

    private void Start()
    {
        // this allows us to listen for the Escape key being pressed
        GameManager.Instance.OnGameUnpaused += GameManager_OnGameUnpaused;
        
        UpdateVisual();
        UpdateVolumeVisual();
        Hide();
    }

    private void GameManager_OnGameUnpaused(object sender, EventArgs e)
    {
        Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
