using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
    [SerializeField] private GameObject hasProgressGameObject;
    [SerializeField] private Image barImage;

    private IHasProgress hasProgress;
    
    private void Start()
    { 
        // can't attach an interface reference in the editor so make a GameObject and get the component
        // then assign it to the interface variable declared above
        hasProgress = hasProgressGameObject.GetComponent<IHasProgress>();
        if (hasProgress == null)
        {
            Debug.LogError("GameObject " + hasProgressGameObject + " does not have a component that implements IHasProgress");
        }
        hasProgress.OnProgressChanged += HasProgress_OnProgressChanged;
        barImage.fillAmount = 0f;
        
        HideProgressBarUi();
    }

    private void HasProgress_OnProgressChanged(object sender, IHasProgress.OnProgressChangedEventArgs e)
    {
        barImage.fillAmount = e.progressAmountNormalized;

        if (e.progressAmountNormalized is 1f or 0f)
        {
            HideProgressBarUi();
        }
        else
        {
            ShowProgressBarUi();
        }
    }

    public void ShowProgressBarUi()
    {
        gameObject.SetActive(true);
    }

    public void HideProgressBarUi()
    {
        gameObject.SetActive(false);
    }
}
