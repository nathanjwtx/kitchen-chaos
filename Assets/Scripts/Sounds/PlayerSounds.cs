using System;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    private Player _player;
    private float _footStepTimer;
    private const float FootStepTimerMax = 0.1f;

    private void Awake()
    {
        _player = GetComponent<Player>();
    }

    private void Update()
    {
        _footStepTimer -= Time.deltaTime;
        if (_footStepTimer < 0f)
        {
            _footStepTimer = FootStepTimerMax;

            if (_player.IsWalking())
            {
                SoundManager.Instance.PlayFootstepSound(_player.transform.position);
            }
        }
    }
}
