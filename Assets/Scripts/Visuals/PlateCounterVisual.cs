using System;
using System.Collections.Generic;
using UnityEngine;

public class PlateCounterVisual : MonoBehaviour
{
    [SerializeField] private Transform counterTopPoint;
    [SerializeField] private Transform plateVisualSinglePrefab;
    [SerializeField] private PlateCounter plateCounter;

    private List<GameObject> _plateVisualSingleList = new ();
    private void Start()
    {
        plateCounter.OnPlateSpawned += PlateCounter_OnPlateSpawned;
        plateCounter.OnPlateRemoved += PlateCounter_OnPlateRemoved;
    }

    private void PlateCounter_OnPlateRemoved(object sender, EventArgs e)
    {
        GameObject plateGameObject = _plateVisualSingleList[^1];
        _plateVisualSingleList.Remove(plateGameObject);
        Destroy(plateGameObject);
    }

    private void PlateCounter_OnPlateSpawned(object sender, EventArgs e)
    {
        Transform plateVisualSingleTransform = Instantiate(plateVisualSinglePrefab, counterTopPoint);

        float plateOffsetY = 0.1f * _plateVisualSingleList.Count;
        plateVisualSingleTransform.localPosition = new Vector3(0, plateOffsetY, 0);
        _plateVisualSingleList.Add(plateVisualSingleTransform.gameObject);
    }
}
