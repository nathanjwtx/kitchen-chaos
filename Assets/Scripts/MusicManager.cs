using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager Instance { get; private set; }
    
    [SerializeField] private AudioSource audioSource;
    
    private const string PLAYER_PREFS_MUSIC_VOLUME = "MusicVolume";

    public float Volume { get; private set; }
    
    private void Awake()
    {
        Instance = this;
        Volume = 0.3f;
        
        Volume = PlayerPrefs.GetFloat(PLAYER_PREFS_MUSIC_VOLUME, 0.3f);
        audioSource.volume = Volume;
    }
    
    public void ChangeVolume()
    {
        Volume += 0.1f;

        if (Volume > 1f)
        {
            Volume = 0f;
        }
        
        audioSource.volume = Volume;
        
        PlayerPrefs.SetFloat(PLAYER_PREFS_MUSIC_VOLUME, Volume);
        PlayerPrefs.Save();
    }
}
