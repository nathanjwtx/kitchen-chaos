using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class DeliveryManager : MonoBehaviour
{
    public static DeliveryManager Instance { get; private set; }
    
    public event EventHandler<OnRecipeSpawnedEventArgs> OnRecipeSpawned;

    public class OnRecipeSpawnedEventArgs : EventArgs
    {
        public RecipeSO RecipeSO { get; set; }
    }
    
    public event EventHandler OnRecipeCompleted;
    public event EventHandler OnRecipeSuccess;
    public event EventHandler OnRecipeFailed;
    
    [SerializeField] private RecipeListSO recipeListSO;
    
    private List<RecipeSO> waitingRecipeSOList = new ();
    
    private float _spawnRecipeTimer;
    private float _spawnRecipeTimerMax = 4f;
    private int _waitingRecipeMax = 4;
    private int _successfulRecipesAmount = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        _spawnRecipeTimer -= Time.deltaTime;
        if (_spawnRecipeTimer <= 0f)
        {
            _spawnRecipeTimer = _spawnRecipeTimerMax;
            if (waitingRecipeSOList.Count < _waitingRecipeMax)
            {
                RecipeSO recipeSO = recipeListSO.recipeSOList[Random.Range(0, recipeListSO.recipeSOList.Count)];
                // Debug.Log(recipeSO.recipeName);
                waitingRecipeSOList.Add(recipeSO);
                OnRecipeSpawned?.Invoke(this, new OnRecipeSpawnedEventArgs {RecipeSO = recipeSO});
            }
        }
    }

    public void DeliverRecipe(PlateKitchenObject plateKitchenObject)
    {
        // loop through each waiting recipe
        foreach (var recipeSO in waitingRecipeSOList)
        {
            var itemsOnPlateInRecipe = 0;
            // loop through each food item on the plate and see if it's in the recipe
            foreach (var foodItemOnPlate in plateKitchenObject.GetKitchenObjectSOList())
            {
                if (!recipeSO.kitchenObjectSOList.Contains(foodItemOnPlate)) break;
                // if the food item on the plate is not in the recipe, break
                foreach (KitchenObjectSO recipeItem in recipeSO.kitchenObjectSOList)
                {
                    if (recipeItem != foodItemOnPlate) continue;
                    itemsOnPlateInRecipe++;
                    break;
                }
            }
            
            if (itemsOnPlateInRecipe != recipeSO.kitchenObjectSOList.Count) continue;
            // player delivered correct recipe
            Debug.Log($"Player delivered correct recipe: {recipeSO.recipeName}");
            _successfulRecipesAmount++;
            waitingRecipeSOList.Remove(recipeSO);
            OnRecipeCompleted?.Invoke(this, EventArgs.Empty);
            OnRecipeSuccess?.Invoke(this, EventArgs.Empty);
            return;
        }
        
        // player did not deliver correct recipe
        Debug.Log("Player did not deliver correct recipe");
        OnRecipeFailed?.Invoke(this, EventArgs.Empty);
    }
    
    public List<RecipeSO> GetWaitingRecipeSOList()
    {
        return waitingRecipeSOList;
    }

    public int GetSuccessfulRecipesAmount()
    {
        return _successfulRecipesAmount;
    }
}