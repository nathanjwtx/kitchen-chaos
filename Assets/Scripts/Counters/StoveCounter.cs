using System;
using UnityEngine;

public class StoveCounter : BaseCounter, IHasProgress
{
    public event EventHandler<IHasProgress.OnProgressChangedEventArgs> OnProgressChanged; 
    public event EventHandler<OnStateChangedEventArgs> OnStateChanged;

    public class OnStateChangedEventArgs : EventArgs
    {
        public State state;
    }
    
    public enum State
    {
        Idle,
        Frying,
        Fried,
        Burned
    }
    
    [SerializeField] private FryingRecipeSO[] fryingRecipeSOArray;
    [SerializeField] private BurningRecipeSO[] burningRecipeSOArray;
    
    private float cookingTimerProgress;
    private float burningTimerProgress;
    private FryingRecipeSO fryingRecipeSo;
    private BurningRecipeSO burningRecipeSo;
    private State currentState;

    private void Start()
    {
        currentState = State.Idle;
    }

    private void Update()
    {
        if (HasKitchenObject())
        {
            switch (currentState)
            {
                case State.Idle:
                    break;
                case State.Frying:
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                    {
                        progressAmountNormalized = (float)cookingTimerProgress / fryingRecipeSo.fryingTimerMax
                    });
                    cookingTimerProgress += Time.deltaTime;
                    if (cookingTimerProgress >= fryingRecipeSo.fryingTimerMax)
                    {
                        // fried. 
                        GetKitchenObject().DestroySelf();
                        
                        KitchenObject.SpawnKitchenObject(fryingRecipeSo.output, this);
                        currentState = State.Fried;
                        burningTimerProgress = 0f;
                        burningRecipeSo = GetBurningRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
                        
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs{ state = currentState });
                    }
                    break;
                case State.Fried:
                    burningTimerProgress += Time.deltaTime;
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                    {
                        progressAmountNormalized = (float)burningTimerProgress / burningRecipeSo.burningTimerMax
                    });
                    if (burningTimerProgress >= burningRecipeSo.burningTimerMax)
                    {
                        // fried. 
                        GetKitchenObject().DestroySelf();
                        
                        KitchenObject.SpawnKitchenObject(burningRecipeSo.output, this);
                        currentState = State.Burned;
                        
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs{ state = currentState });

                        // setting the argument to 0 essentially hides the progress bar
                        OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                        {
                            progressAmountNormalized = 0f
                        });
                    }
                    break;
                case State.Burned:
                    break;
            }
        }
    }

    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            // Player has not picked up any object
            if (player.HasKitchenObject())
            {
                // Player has picked up something, and we are testing it can be fried
                if (HasRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO()))
                {
                    player.GetKitchenObject().SetKitchenObjectParent(this);
                    fryingRecipeSo = GetFryingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());

                    currentState = State.Frying;
                    cookingTimerProgress = 0f;
                    
                    OnStateChanged?.Invoke(this, new OnStateChangedEventArgs{ state = currentState });
                    // fire the event to update the progress bar
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                    {
                        // need to cast to float to ensure a value between 0 and 1 else and int / int will return 0
                        progressAmountNormalized = (float)cookingTimerProgress / fryingRecipeSo.fryingTimerMax
                    });

                    // FryingRecipeSO fryingRecipeSO = GetCuttingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
                    // send the event with updated progress
                    // OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs
                    // {
                    // need to cast to float to ensure a value between 0 and 1 else and int / int will return 0
                    // progressAmountNormalized = (float)cuttingProgress / fryingRecipeSO.cuttingProgressMax
                    // });
                }
            }
            else
            {
                // Player has not picked up anything
            }
        }
        else
        {
            // there is a kitchen object here
            if (player.HasKitchenObject())
            {
                // Player has picked up something so check if it is a plate
                if (!player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject)) return;
                // Player has picked up a plate so try to add an ingredient to the plate
                if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO()))GetKitchenObject().DestroySelf();
                // after the patty is picked up reset state
                currentState = State.Idle;
                OnStateChanged?.Invoke(this, new OnStateChangedEventArgs{ state = currentState });

                OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                {
                    progressAmountNormalized = 0f
                });
            }
            else
            {
                // Player has nothing in hand so can pick up the kitchen object
                GetKitchenObject().SetKitchenObjectParent(player);
                
                // after the patty is picked up reset state
                currentState = State.Idle;
                OnStateChanged?.Invoke(this, new OnStateChangedEventArgs{ state = currentState });

                OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                {
                    progressAmountNormalized = 0f
                });
            }
        }
        
    }
    
    private bool HasRecipeWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        FryingRecipeSO fryingRecipeSO = GetFryingRecipeSOWithInput(inputKitchenObjectSO);
        return fryingRecipeSO != null;
    }

    private KitchenObjectSO GetOutputKitchenObjectSO(KitchenObjectSO inputKitchenObjectSO)
    {
        FryingRecipeSO fryingRecipeSO = GetFryingRecipeSOWithInput(inputKitchenObjectSO);
        if (fryingRecipeSO != null)
        {
            return fryingRecipeSO.output;
        }
        return null;
    }

    private FryingRecipeSO GetFryingRecipeSOWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        foreach (FryingRecipeSO fryingRecipeSo in fryingRecipeSOArray)
        {
            if (fryingRecipeSo.input == inputKitchenObjectSO)
            {
                return fryingRecipeSo;
            }
        }
        return null;
    }
    
    private BurningRecipeSO GetBurningRecipeSOWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        foreach (BurningRecipeSO burningRecipeSO in burningRecipeSOArray)
        {
            if (burningRecipeSO.input == inputKitchenObjectSO)
            {
                return burningRecipeSO;
            }
        }
        return null;
    }
}
