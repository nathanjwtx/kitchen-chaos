using System;
using UnityEngine;

public class CuttingCounter : BaseCounter, IHasProgress
{
    // if we had used OnCut that would have added the event handler to each counter individually
    // meaning we would have had to add the event handler to each counter. Using a static event
    // means we don't have to add the event handler to each counter and it won't override the OnCut functionality
    public static event EventHandler OnAnyCut;

    /// <summary>
    /// Ensures the static event is cleared when the class is destroyed.
    /// It can then be successfully recreated again.
    /// </summary>
    public new static void ResetStaticData()
    {
        OnAnyCut = null;
    }
    
    public event EventHandler<IHasProgress.OnProgressChangedEventArgs> OnProgressChanged;
    public event EventHandler OnCut;
    
    [SerializeField] private CuttingRecipeSO[] cutKitchenObjectSoArray;

    private int cuttingProgress;
    
    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            // Player has not picked up any object
            if (player.HasKitchenObject())
            {
                // Player has picked up something, and we are testing it can be cut
                if (HasRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO()))
                {
                    cuttingProgress = 0;
                    player.GetKitchenObject().SetKitchenObjectParent(this);
                
                    CuttingRecipeSO cuttingRecipeSo = GetCuttingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
                    // send the event with updated progress
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
                    {
                        // need to cast to float to ensure a value between 0 and 1 else and int / int will return 0
                        progressAmountNormalized = (float)cuttingProgress / cuttingRecipeSo.cuttingProgressMax
                    });
                }
            }
            else
            {
                // Player has not picked up anything
            }
        }
        else
        {
            // there is a kitchen object here
            if (player.HasKitchenObject())
            {
                // Player has picked up something so check if it is a plate
                if (!player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject)) return;
                // Player has picked up a plate so try to add an ingredient to the plate
                if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO()))GetKitchenObject().DestroySelf();
            }
            else
            {
                // Player has nothing in hand so can pick up the kitchen object
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }

    // Interacts with the player using an alternate method.
    // Checks if the player has a kitchen object and a recipe with the input kitchen object.
    // If true, cuts the kitchen object and spawns the output kitchen object.
    // Parameters:
    //   player: The player interacting with the kitchen object.
    public override void InteractAlternate(Player player)
    {
        if (HasKitchenObject() && HasRecipeWithInput(GetKitchenObject().GetKitchenObjectSO()))
        {
            cuttingProgress++;
            OnCut?.Invoke(this, EventArgs.Empty);
            OnAnyCut?.Invoke(this, EventArgs.Empty);
            CuttingRecipeSO cuttingRecipeSo = GetCuttingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
            OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs
            {
                // need to cast to float to ensure a value between 0 and 1 else and int / int will return 0
                progressAmountNormalized = (float)cuttingProgress / cuttingRecipeSo.cuttingProgressMax
            });
            if (cuttingProgress >= cuttingRecipeSo.cuttingProgressMax)
            {
                KitchenObjectSO outputKitchenObjectSO = GetCutKitchenObjectSO(GetKitchenObject().GetKitchenObjectSO());
                // there is an object so let's cut it
                GetKitchenObject().DestroySelf();
                
                KitchenObject.SpawnKitchenObject(outputKitchenObjectSO, this);
            }
        }
    }

    private KitchenObjectSO GetCutKitchenObjectSO(KitchenObjectSO inputKitchenObjectSO)
    {
        CuttingRecipeSO cuttingRecipeSO = GetCuttingRecipeSOWithInput(inputKitchenObjectSO);
        if (cuttingRecipeSO != null)
        {
            return cuttingRecipeSO.output;
        }
        return null;
    }

    private bool HasRecipeWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        CuttingRecipeSO cuttingRecipeSO = GetCuttingRecipeSOWithInput(inputKitchenObjectSO);
        return cuttingRecipeSO != null;
    }

    private CuttingRecipeSO GetCuttingRecipeSOWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        foreach (CuttingRecipeSO cuttingRecipeSo in cutKitchenObjectSoArray)
        {
            if (cuttingRecipeSo.input == inputKitchenObjectSO)
            {
                return cuttingRecipeSo;
            }
        }
        return null;
    }
}
