﻿using UnityEngine;

public class ClearCounter : BaseCounter
{
    [SerializeField] private KitchenObjectSO kitchenObjectSO;

    // Method to perform the interaction, and instantiate the kitchen object if it doesn't exist.
    // Only one kitchen object can exist at a time.
    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            // Player has not picked up any object
            if (player.HasKitchenObject())
            {
                // Player has picked up something
                player.GetKitchenObject().SetKitchenObjectParent(this);
            }
            else
            {
                // Player has not picked up anything
            }
        }
        else
        {
            // there is a kitchen object here
            if (player.HasKitchenObject())
            {
                // Player has picked up something so check if it is a plate
                if (player.GetKitchenObject().TryGetPlate(out PlateKitchenObject playerPlate))
                {
                    // Player has picked up a plate so try to add an ingredient to the plate
                    if (playerPlate.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO())) GetKitchenObject().DestroySelf();
                }
                else
                {
                    // Player has picked up something other than a plate
                    // check if a plate is on the counter
                    // in the out parameter playerPlate we don't need to declare the object type as already declared above, or give it a new name
                    if (GetKitchenObject().TryGetPlate(out playerPlate))
                    {
                        // Counter is holding a plate so add what the player picked up to the plate
                        if (playerPlate.TryAddIngredient(player.GetKitchenObject().GetKitchenObjectSO()))
                        {
                            player.GetKitchenObject().DestroySelf();
                        }
                    }
                }
            }
            else
            {
                // Player has nothing in hand so can pick up the kitchen object
                GetKitchenObject().SetKitchenObjectParent(player);
            }
        }
    }
}