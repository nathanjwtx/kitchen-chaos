using System;
using UnityEngine;

public class BaseCounter : MonoBehaviour, IKitchenObjectParent
{
    public static event EventHandler OnDropSomething;
    
    public static void ResetStaticData()
    {
        OnDropSomething = null;
    }
    
    [SerializeField] private Transform counterTopPoint;
    
    private KitchenObject _kitchenObject;

    public virtual void Interact(Player player) { Debug.LogError("BaseCounter.Interact() is not implemented");}

    public virtual void InteractAlternate(Player player)
    {
     // Debug.LogError("BaseCounter.InteractAlternate() is not implemented");
    }
    
    public Transform GetKitchenObjectFollowTransform()
    {
        return counterTopPoint;
    }

    public void SetKitchenObject(KitchenObject kitchenObject)
    {
        _kitchenObject = kitchenObject;
        if (kitchenObject)
        {
            OnDropSomething?.Invoke(this, EventArgs.Empty);
        }
    }

    public KitchenObject GetKitchenObject()
    {
        return _kitchenObject;
    }

    public void ClearKitchenObject()
    {
        _kitchenObject = null;
    }

    public bool HasKitchenObject()
    {
        return _kitchenObject;
    }
}
