using UnityEngine;

public class KitchenObject : MonoBehaviour
{
    [SerializeField] private KitchenObjectSO kitchenObjectSO;

    private IKitchenObjectParent _kitchenObjectParent;

    public KitchenObjectSO GetKitchenObjectSO()
    {
        return kitchenObjectSO;
    }

    public void SetKitchenObjectParent(IKitchenObjectParent kitchenObjectParent)
    {
        // if there is already a kitchen object, clear it
        _kitchenObjectParent?.ClearKitchenObject();

        // set the new clear counter
        _kitchenObjectParent = kitchenObjectParent;
        // throw an error if the new counter already has a kitchen object
        if (kitchenObjectParent.HasKitchenObject()) Debug.LogError("Kitchen object parent already has a kitchen object");
        // tell the new parent it has a new kitchen object
        kitchenObjectParent.SetKitchenObject(this);
        // update the visual position
        transform.parent = kitchenObjectParent.GetKitchenObjectFollowTransform();
        transform.localPosition = Vector3.zero;
    }

    public IKitchenObjectParent GetKitchenObjectParent()
    {
        return _kitchenObjectParent;
    }

    public void DestroySelf()
    {
        _kitchenObjectParent.ClearKitchenObject();
        Destroy(gameObject);
    }

    // Spawns a new KitchenObject based on the provided KitchenObjectSO and sets its parent to the given IKitchenObjectParent.
    public static KitchenObject SpawnKitchenObject(KitchenObjectSO kitchenObjectSO, IKitchenObjectParent kitchenObjectParent)
    {
            // don't need the parent transform as it will be set when SetKitchenObjectParent is called
            Transform kitchenObjectTransform = Instantiate(kitchenObjectSO.prefab);
            // spawn the object and give it to the parent, e.g. player
            KitchenObject kitchenObject = kitchenObjectTransform.GetComponent<KitchenObject>();
            kitchenObject.SetKitchenObjectParent(kitchenObjectParent);
            return kitchenObject;
    }

    // Try to get a PlateKitchenObject, if successful, return true and assign the object to the output parameter.
    // Otherwise, return false.
    public bool TryGetPlate(out PlateKitchenObject plateKitchenObject)
    {
        if (this is PlateKitchenObject plateKO)
        {
            plateKitchenObject = plateKO;
            return true;
        }
        plateKitchenObject = null;
        return false;
    }
}
