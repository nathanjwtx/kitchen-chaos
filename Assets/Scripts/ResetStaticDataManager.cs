using System;
using UnityEngine;

public class ResetStaticDataManager : MonoBehaviour
{
    private void Awake()
    {
        // This class is on the MainMenuScene and resets all static events so the game can be played again.
        // Errors can occur if these are not reset as they attempt to be recreated.
        CuttingCounter.ResetStaticData();
        BaseCounter.ResetStaticData();
        TrashCounter.ResetStaticData();
    }
}
