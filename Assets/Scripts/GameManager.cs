using System;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    
    public static GameManager Instance { get; private set; }
    
    public event EventHandler OnGameStateChanged;
    public event EventHandler OnGamePaused;
    public event EventHandler OnGameUnpaused;
    
    private void GameInput_OnPauseAction(object sender, EventArgs e)
    {
        TogglePauseGame();
    }
    
    private enum GameState
    {
        WaitingToStart,
        CountdownToStart,
        GamePlaying,
        GameOver,
        GamePaused
    }
    
    [SerializeField] private float waitingToStartTimer = 1f;
    [SerializeField] private float countdownToStartTimer = 3f;
    [SerializeField] private float gamePlayingTimer = 10f;
    
    private GameState _gameState = GameState.WaitingToStart;
    public float GamePlayingCountdown { get; private set; }

    private void Start()
    {
        GameInput.Instance.OnPauseAction += GameInput_OnPauseAction;
    }

    private void Awake()
    {
        Instance = this;
        GamePlayingCountdown = gamePlayingTimer;
    }

    private void Update()
    {
        switch (_gameState)
        {
            case GameState.WaitingToStart:
                waitingToStartTimer -= Time.deltaTime;
                if (waitingToStartTimer < 0)
                {
                    _gameState = GameState.CountdownToStart;
                    OnGameStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.GamePlaying:
                GamePlayingCountdown -= Time.deltaTime;
                if (GamePlayingCountdown < 0)
                {
                    _gameState = GameState.GameOver;
                    OnGameStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.CountdownToStart:
                countdownToStartTimer -= Time.deltaTime;
                if (countdownToStartTimer < 0)
                {
                    _gameState = GameState.GamePlaying;
                    OnGameStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.GameOver:
                break;
        }
        // Debug.Log(_gameState);
    }

    public bool IsGamePlaying()
    {
        return _gameState == GameState.GamePlaying;
    }

    public bool IsCountdownToStartActive()
    {
        return _gameState == GameState.CountdownToStart;
    }

    public float GetCountdownToStartTimer()
    {
        return countdownToStartTimer;
    }

    public bool IsGameOver()
    {
        return _gameState == GameState.GameOver;
    }

    public float GetGamePlayingTimer()
    {
        return gamePlayingTimer;
    }

    public void TogglePauseGame()
    {
        if (_gameState == GameState.GamePlaying)
        {
            Time.timeScale = 0f;
            _gameState = GameState.GamePaused;            
            OnGamePaused?.Invoke(this, EventArgs.Empty);
        }
        else if (_gameState == GameState.GamePaused)
        {
            Time.timeScale = 1f;
            _gameState = GameState.GamePlaying;
            OnGameUnpaused?.Invoke(this, EventArgs.Empty);
        }
    }
}
